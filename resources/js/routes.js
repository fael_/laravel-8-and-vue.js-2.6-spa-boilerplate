import VueRouter from 'vue-router';
import Sample from './components/Sample.vue';

const routes = [
    {
        path: "/",
        component: Sample,
        name: "example"
    },
]

const router = new VueRouter({
    mode: 'history',
    routes 
});

export default router;