**Installation**

1. `git clone https://gitlab.com/fael_/laravel-8-and-vue.js-2.6-spa-boilerplate.git`
2. _Go to project directory._
3. `npm install --global cross-env`
4. `npm install --no-bin-links`
5. `cp .env.example .env`
6. `composer install`
7. `composer update`
8. `npm run dev`
9. `php artisan key:generate`
10. `php artisan serve`
11. `npm run watch`
